package dj.indie.ccandroid.APIs;

import dj.indie.ccandroid.Models.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherAPIs {

    @GET("forecast.json?")
    Call<WeatherResponse> getForecastWeather(@Query("key") String key, @Query("q") String city, @Query("days") String days);

    @GET("current.json?")
    Call<WeatherResponse> getCurrentWeather(@Query("key") String key, @Query("q") String city);

}
