package dj.indie.ccandroid.APIs;

import dj.indie.ccandroid.Models.RestaurantResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestaurantAPIs {

    @GET("json?")
    Call<RestaurantResponse> getRestaurants(@Query("query") String query, @Query("location") String location, @Query("key") String key);

}
