package dj.indie.ccandroid.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import dj.indie.ccandroid.Adapters.RestaurantsListAdapter;
import dj.indie.ccandroid.Delegates.RestDelegate_Module_Fragment;
import dj.indie.ccandroid.Models.RestaurantResponse;
import dj.indie.ccandroid.Modules.RestaurantModule;
import dj.indie.ccandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantFragment extends Fragment implements RestDelegate_Module_Fragment {
    private View v;
    ListView lv;
    private RestaurantModule restaurantModule = new RestaurantModule(this);

    public RestaurantFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_restaurant, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        v = view;
        restaurantModule.getRestaurantData();
        initializeViews();
    }

    private void initializeViews() {
        lv = v.findViewById(R.id.lv_rest);
    }

    @Override
    public void onRestDataSuccess(RestaurantResponse restaurantResponse) {
        RestaurantsListAdapter adapter = new RestaurantsListAdapter(getContext(), R.layout.bp_restaurant_item, restaurantResponse.getResults());
        lv.setAdapter(adapter);
    }

    @Override
    public void onRestDataFailure() {

    }
}
