package dj.indie.ccandroid.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import dj.indie.ccandroid.Adapters.ForecastWeatherAdapter;
import dj.indie.ccandroid.Delegates.WeatherDelegate_Module_Fragment;
import dj.indie.ccandroid.Models.Current;
import dj.indie.ccandroid.Models.Forecast;
import dj.indie.ccandroid.Models.Location;
import dj.indie.ccandroid.Models.WeatherResponse;
import dj.indie.ccandroid.Modules.WeatherModule;
import dj.indie.ccandroid.R;

import static dj.indie.ccandroid.Utilities.LoggingUtility.log;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFragment extends Fragment implements WeatherDelegate_Module_Fragment {
    private WeatherModule weatherModule = new WeatherModule(this);
    private View view;
    private TextView tv_weather_city, tv_weather_day, tv_weather_status, tv_weather_temp, tv_weather_percipitation, tv_weather_humidity, tv_weather_windSpeed;
    private ImageView iv_weather_image;
    private RecyclerView rv_forecast;

    public WeatherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        weatherModule.getForecastWeatherData();
        this.view = view;
        initializeViews();
    }

    private void initializeViews() {
        this.tv_weather_city = this.view.findViewById(R.id.weather_city);
        this.tv_weather_day = this.view.findViewById(R.id.weather_day);
        this.tv_weather_status = this.view.findViewById(R.id.weather_status);
        this.tv_weather_temp = this.view.findViewById(R.id.weather_temp);
        this.tv_weather_percipitation = this.view.findViewById(R.id.weather_percipitation);
        this.tv_weather_humidity = this.view.findViewById(R.id.weather_humidity);
        this.tv_weather_windSpeed = this.view.findViewById(R.id.weather_windSpeed);
        this.iv_weather_image = this.view.findViewById(R.id.weather_image);
        this.rv_forecast = this.view.findViewById(R.id.weather_rv);
        Button btn = this.view.findViewById(R.id.btn_restaurants);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, new RestaurantFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    public void setWeatherData(WeatherResponse weatherResponse) {
//        log(getClass(), weatherResponse.toString());
        setLocationDetails(weatherResponse.getLocation());
        setCurrentDetails(weatherResponse.getCurrent());
        setForecastDetails(weatherResponse.getForecast());
    }

    private void setForecastDetails(Forecast forecast) {
//        log(getContext(), forecast.getForecastday().size());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        ForecastWeatherAdapter adapter = new ForecastWeatherAdapter(forecast.getForecastday(), getContext());
        this.rv_forecast.setLayoutManager(linearLayoutManager);
        this.rv_forecast.setAdapter(adapter);
    }

    private void setCurrentDetails(Current current) {
        this.tv_weather_status.setText(current.getCondition().getText());
        this.tv_weather_temp.setText("" + Math.round(current.getTempC()) + "°C");
        Picasso.get().load("http:" + current.getCondition().getIcon().replace("64x64", "128x128")).into(this.iv_weather_image);
        this.tv_weather_percipitation.setText("" + current.getPrecipIn() + "%");
        this.tv_weather_humidity.setText("" + current.getHumidity() + "%");
        this.tv_weather_windSpeed.setText("" + current.getWindKph() + "km/h");
    }

    private void setLocationDetails(Location location) {
        this.tv_weather_city.setText(location.getName());
        this.tv_weather_day.setText(getWeekDay(location.getLocaltime()));
    }

    private String getWeekDay(String localtime) {
        DateFormat outputFormat = new SimpleDateFormat("EEEE", Locale.US);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
        Date date = null;
        try {
            date = inputFormat.parse(localtime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);
        return outputText;
    }

    @Override
    public void onForecastSuccess(WeatherResponse weatherResponse) {
        setWeatherData(weatherResponse);
    }

    @Override
    public void onForecastFailure() {

    }
}
