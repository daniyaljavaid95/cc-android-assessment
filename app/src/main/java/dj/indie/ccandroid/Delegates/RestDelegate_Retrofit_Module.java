package dj.indie.ccandroid.Delegates;

import dj.indie.ccandroid.Models.RestaurantResponse;

public interface RestDelegate_Retrofit_Module {

    void onRestDataSuccess(RestaurantResponse restaurantResponse);

    void onRestDataFailure();
}
