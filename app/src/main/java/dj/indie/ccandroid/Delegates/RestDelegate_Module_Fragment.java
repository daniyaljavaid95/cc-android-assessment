package dj.indie.ccandroid.Delegates;

import dj.indie.ccandroid.Models.RestaurantResponse;

public interface RestDelegate_Module_Fragment {
    void onRestDataSuccess(RestaurantResponse restaurantResponse);

    void onRestDataFailure();
}
