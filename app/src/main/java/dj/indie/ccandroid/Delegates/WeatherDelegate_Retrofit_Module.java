package dj.indie.ccandroid.Delegates;

import dj.indie.ccandroid.Models.WeatherResponse;

public interface WeatherDelegate_Retrofit_Module {

    void onForecastSuccess(WeatherResponse weatherResponse);

    void onForecastFailure();
}
