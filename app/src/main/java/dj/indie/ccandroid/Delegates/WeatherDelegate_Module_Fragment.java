package dj.indie.ccandroid.Delegates;

import dj.indie.ccandroid.Models.WeatherResponse;

public interface WeatherDelegate_Module_Fragment {
    void onForecastSuccess(WeatherResponse weatherResponse);

    void onForecastFailure();
}
