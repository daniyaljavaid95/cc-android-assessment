
package dj.indie.ccandroid.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Geometry {

    @SerializedName("location")
    @Expose
    private Rest_Location location;
    @SerializedName("viewport")
    @Expose
    private Viewport viewport;

    public Rest_Location getLocation() {
        return location;
    }

    public void setLocation(Rest_Location location) {
        this.location = location;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }

    @Override
    public String toString() {
        return "Geometry{" +
                "location=" + location +
                ", viewport=" + viewport +
                '}';
    }
}
