package dj.indie.ccandroid.Controllers;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dj.indie.ccandroid.APIs.RestaurantAPIs;
import dj.indie.ccandroid.APIs.WeatherAPIs;
import dj.indie.ccandroid.Constants.Constants;
import dj.indie.ccandroid.Delegates.RestDelegate_Retrofit_Module;
import dj.indie.ccandroid.Delegates.WeatherDelegate_Retrofit_Module;
import dj.indie.ccandroid.Models.RestaurantResponse;
import dj.indie.ccandroid.Models.WeatherResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestaurantController implements Callback<RestaurantResponse> {
    private RestDelegate_Retrofit_Module delegate;
    private Constants constants = new Constants();

    public RestaurantController(RestDelegate_Retrofit_Module delegate) {
        this.delegate = delegate;
    }

    private RestaurantAPIs builder(String url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RestaurantAPIs restaurantAPIs = retrofit.create(RestaurantAPIs.class);
        return restaurantAPIs;
    }

    public void getRestaurantsData(String query, String coordinates) {
        this.builder(constants.gmapApiBaseURL).getRestaurants(query, coordinates, constants.gmapApiKey).enqueue(this);
    }

    @Override
    public void onResponse(Call<RestaurantResponse> call, Response<RestaurantResponse> response) {
        this.delegate.onRestDataSuccess(response.body());
    }

    @Override
    public void onFailure(Call<RestaurantResponse> call, Throwable t) {

    }
}
