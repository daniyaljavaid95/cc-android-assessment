package dj.indie.ccandroid.Controllers;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dj.indie.ccandroid.APIs.WeatherAPIs;
import dj.indie.ccandroid.Constants.Constants;
import dj.indie.ccandroid.Delegates.WeatherDelegate_Retrofit_Module;
import dj.indie.ccandroid.Models.WeatherResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static dj.indie.ccandroid.Utilities.LoggingUtility.log;

public class WeatherController implements Callback<WeatherResponse> {
    private WeatherDelegate_Retrofit_Module delegate;
    private Constants constants = new Constants();

    public WeatherController(WeatherDelegate_Retrofit_Module delegate) {
        this.delegate = delegate;
    }

    private WeatherAPIs builder(String url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        WeatherAPIs weatherAPI = retrofit.create(WeatherAPIs.class);
        return weatherAPI;
    }

    public void getForecastData(String city, String days) {
        this.builder(constants.weatherApiBaseURL).getForecastWeather(constants.weatherApiKey, city, days).enqueue(this);
    }

    @Override
    public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
        this.delegate.onForecastSuccess(response.body());
    }

    @Override
    public void onFailure(Call<WeatherResponse> call, Throwable t) {
        this.delegate.onForecastFailure();
    }
}
