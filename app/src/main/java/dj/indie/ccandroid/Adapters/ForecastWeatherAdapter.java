package dj.indie.ccandroid.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import dj.indie.ccandroid.Models.Forecastday;
import dj.indie.ccandroid.R;

public class ForecastWeatherAdapter extends RecyclerView.Adapter<ForecastWeatherAdapter.ViewHolder> {
    private List<Forecastday> forecastdayList;
    private Context context;

    public ForecastWeatherAdapter(List<Forecastday> forecastdayList, Context context) {
        this.forecastdayList = forecastdayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bp_forecast_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.tv_min.setText("" + forecastdayList.get(i).getDay().getMintempC() + "°");
        viewHolder.tv_day.setText(getWeekDay(forecastdayList.get(i).getDate()).toUpperCase());
        viewHolder.tv_max.setText("" + forecastdayList.get(i).getDay().getMaxtempC() + "°");
        Picasso.get().load("http:" + forecastdayList.get(i).getDay().getCondition().getIcon().replace("64x64", "128x128")).into(viewHolder.iv_iamge);
    }

    @Override
    public int getItemCount() {
        return forecastdayList.size();
    }

    private String getWeekDay(String localtime) {
        DateFormat outputFormat = new SimpleDateFormat("EE", Locale.US);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = null;
        try {
            date = inputFormat.parse(localtime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);
        return outputText;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_day, tv_max, tv_min;
        ImageView iv_iamge;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_day = itemView.findViewById(R.id.fc_day);
            tv_max = itemView.findViewById(R.id.fc_max_temp);
            tv_min = itemView.findViewById(R.id.fc_min_temp);
            iv_iamge = itemView.findViewById(R.id.fc_image);
        }
    }
}
