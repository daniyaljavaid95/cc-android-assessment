package dj.indie.ccandroid.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dj.indie.ccandroid.Models.Result;
import dj.indie.ccandroid.R;

public class RestaurantsListAdapter extends ArrayAdapter {
    List<Result> results;
    Context mContext;
    int res;

    public RestaurantsListAdapter(@NonNull Context context, int resource, @NonNull List<Result> objects) {
        super(context, resource, objects);
        results = objects;
        mContext = context;
        res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Result result = results.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(res, parent, false);
        }
        ImageView iv = convertView.findViewById(R.id.rest_image);
        TextView tv_restName = (TextView) convertView.findViewById(R.id.tv_restName);
        TextView tv_reviews = (TextView) convertView.findViewById(R.id.tv_reviews);
        TextView tv_rating = (TextView) convertView.findViewById(R.id.tv_rating);
        RatingBar bar = convertView.findViewById(R.id.ratingBar);
        bar.setRating(Float.parseFloat("" + result.getRating()));
        tv_restName.setText("" + result.getName());
        tv_reviews.setText("" + result.getUserRatingsTotal() + " Reviews");
        tv_rating.setText("" + result.getRating());
        Picasso.get().load(result.getIcon()).into(iv);

        // Return the completed view to render on screen
        return convertView;
    }

}
