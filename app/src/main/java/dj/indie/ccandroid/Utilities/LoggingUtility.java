package dj.indie.ccandroid.Utilities;

import android.content.Context;
import android.util.Log;

public class LoggingUtility {
    public static void log(Context c, String s) {
        Log.e(c.getClass().getSimpleName(), s);
    }

    public static void log(Context c, int i) {
        Log.e(c.getClass().getSimpleName(), "" + i);
    }

    public static void log(Class c, String s) {
        Log.e(c.getClass().getSimpleName(), "" + s);
    }

    public static void log(Class c, int i) {
        Log.e(c.getClass().getSimpleName(), "" + i);
    }
}
