package dj.indie.ccandroid.Constants;

public class Constants {

    public String weatherApiKey = "7bffca4de14d44c6b28115337190903";
    public String weatherApiBaseURL = "http://api.apixu.com/v1/";
    private String weatherApiParamDaysCount = "&days=5/";
    private String weatherApiParamLocation = "&q=Karachi/";
    private String weatherApiCurrentData = "current.json";
    public String weatherApiForecastData = "forecast.json";

    public String gmapApiBaseURL = "https://maps.googleapis.com/maps/api/place/textsearch/";
    public String gmapApiKey = "AIzaSyCSbNG0PLVjgKMEwGTOkubXlFfljGdFhtE";
    private String gmapApiQuery = "?query=5star+restaurant";

    public String currentWeatherReqCode = "111";
    public String forecastWeatherReqCode = "222";
    public String restaurantsReqCode = "333";

    private String restaurantsURL = "";
//    public String currentWeatherURL;
//    public String forecastWeatherURL;

//    public Constants() {
//        this.currentWeatherURL = "" + (weatherApiBaseURL) + (weatherApiCurrentData) + "?key=" + (weatherApiKey) + (weatherApiParamLocation);
//        this.forecastWeatherURL = "" + (weatherApiBaseURL) + (weatherApiForecastData) + "?key=" + (weatherApiKey) + (weatherApiParamLocation) + (weatherApiParamDaysCount);
//    }

//    public String getRestaurantApiURL(String coordiantes) {
//        String url = "" + (gmapApiBaseURL) + (gmapApiQuery) + "&location=" + (coordiantes) + "&key=" + (gmapApiKey);
//        this.restaurantsURL = url;
//        return url;
//    }

}
