package dj.indie.ccandroid.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import dj.indie.ccandroid.Constants.Constants;
import dj.indie.ccandroid.Fragments.RestaurantFragment;
import dj.indie.ccandroid.Fragments.WeatherFragment;
import dj.indie.ccandroid.R;

import static dj.indie.ccandroid.Utilities.LoggingUtility.log;

public class MainActivity extends AppCompatActivity {
    Constants constants = new Constants();
    int frameLayout = R.id.frameLayout;
    WeatherFragment weatherFragment = new WeatherFragment();
    RestaurantFragment restaurantFragment = new RestaurantFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideActionBar();
        displayWeatherFragment();
//        displayRestaurantFragment();
    }

    public void displayWeatherFragment() {
        getSupportFragmentManager().beginTransaction().replace(frameLayout, weatherFragment).commit();
    }

    public void displayRestaurantFragment() {
        getSupportFragmentManager().beginTransaction().replace(frameLayout, restaurantFragment).commit();
    }

    public void hideActionBar() {
        getSupportActionBar().hide();
    }

}
