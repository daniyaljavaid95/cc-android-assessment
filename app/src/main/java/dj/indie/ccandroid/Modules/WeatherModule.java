package dj.indie.ccandroid.Modules;

import android.util.Log;

import dj.indie.ccandroid.Controllers.WeatherController;
import dj.indie.ccandroid.Delegates.WeatherDelegate_Module_Fragment;
import dj.indie.ccandroid.Delegates.WeatherDelegate_Retrofit_Module;
import dj.indie.ccandroid.Models.WeatherResponse;

import static dj.indie.ccandroid.Utilities.LoggingUtility.log;

public class WeatherModule implements WeatherDelegate_Retrofit_Module {
    private WeatherDelegate_Module_Fragment delegate;

    public WeatherModule(WeatherDelegate_Module_Fragment delegate) {
        this.delegate = delegate;
    }

    private String city = "Karachi";
    private String days = "10";

    public void getForecastWeatherData() {
        WeatherController controller = new WeatherController(this);
        controller.getForecastData(city, days);
    }

    @Override
    public void onForecastSuccess(WeatherResponse weatherResponse) {
        this.delegate.onForecastSuccess(weatherResponse);
    }

    @Override
    public void onForecastFailure() {
        this.delegate.onForecastFailure();
    }
}
