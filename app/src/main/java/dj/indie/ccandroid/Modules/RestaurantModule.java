package dj.indie.ccandroid.Modules;

import dj.indie.ccandroid.Controllers.RestaurantController;
import dj.indie.ccandroid.Controllers.WeatherController;
import dj.indie.ccandroid.Delegates.RestDelegate_Module_Fragment;
import dj.indie.ccandroid.Delegates.RestDelegate_Retrofit_Module;
import dj.indie.ccandroid.Delegates.WeatherDelegate_Module_Fragment;
import dj.indie.ccandroid.Delegates.WeatherDelegate_Retrofit_Module;
import dj.indie.ccandroid.Models.RestaurantResponse;
import dj.indie.ccandroid.Models.WeatherResponse;

public class RestaurantModule implements RestDelegate_Retrofit_Module {
    private RestDelegate_Module_Fragment delegate;

    public RestaurantModule(RestDelegate_Module_Fragment delegate) {
        this.delegate = delegate;
    }

    private String query = "5star+restaurant";
    private String coordinates = "24.8607,67.0011";

    public void getRestaurantData() {
        RestaurantController controller = new RestaurantController(this);
        controller.getRestaurantsData(query, coordinates);
    }

    @Override
    public void onRestDataSuccess(RestaurantResponse restaurantResponse) {
        this.delegate.onRestDataSuccess(restaurantResponse);
    }

    @Override
    public void onRestDataFailure() {

    }
}
